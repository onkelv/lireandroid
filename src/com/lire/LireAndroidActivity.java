package com.lire;

import java.io.File;
import java.io.IOException;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.DocumentBuilderFactory;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.ImageSearcherFactory;
import net.semanticmetadata.lire.android.utils.BitmapUtils;
import net.semanticmetadata.lire.android.utils.GeneralHelper;

import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class LireAndroidActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        try {
			shortLireTest();
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (LockObtainFailedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    
    private void shortLireTest() throws CorruptIndexException, LockObtainFailedException, IOException{
		// Create an appropriate DocumentBuilder
		DocumentBuilder builder = DocumentBuilderFactory
				.getCEDDDocumentBuilder();
		// That's the way it is done with Lucene 3.0 - supported with LIRe v0.8
		File folder = new File(Environment.getExternalStorageDirectory()
				+ "/lire_luceneindex");
		if (!folder.exists()) {
			folder.mkdir();
		}
		IndexWriter iw = new IndexWriter(FSDirectory.open(folder),
				new SimpleAnalyzer(), true,
				IndexWriter.MaxFieldLength.UNLIMITED);
		String[] testFiles = GeneralHelper.getPictureFileList();
		for (int i = 0; i < 5; i++) {
			// Build the Lucene Documents
			Bitmap b = BitmapUtils.decodeFile(
					new File(GeneralHelper.getStandardPictureVideoPath() + "/"
							+ testFiles[i]), 640);
			Document doc = builder.createDocument(b, testFiles[i]);
			// Add the Documents to the index
			iw.addDocument(doc);
		}
		iw.optimize();
		iw.close();

		// [...] start snippet --------
		// That's for Lucene v3.0+
		IndexReader reader = IndexReader.open(FSDirectory.open(
				folder));
		// three different possible versions ... for finding the 10 most
		// relevant pictures
		ImageSearcher searcher = ImageSearcherFactory
				.createCEDDImageSearcher(3);
		// ImageSearcher searcher =
		// ImageSearcherFactory.createWeightedSearcher(10, 0.8f, 0.0f, 1.0f);
		// ImageSearcher searcher =
		// ImageSearcherFactory.createWeightedSearcher(10, 0.0f, 1.0f, 0.0f);
		String searchImageFile = GeneralHelper.getStandardPictureVideoPath()
				+ "/" + GeneralHelper.getPictureFileList()[0];
		Bitmap bitmap = BitmapUtils.decodeFile(new File(searchImageFile), 640);
		ImageSearchHits hits = null;
		hits = searcher.search(bitmap, reader);
		for (int i = 0; i < 3; i++) {
			System.out.println(hits.score(i)
					+ ": "
					+ hits.doc(i)
							.getField(DocumentBuilder.FIELD_NAME_IDENTIFIER)
							.stringValue());
			Log.i(LireAndroidActivity.class.getName(),hits.score(i)
					+ ": "
					+ hits.doc(i)
							.getField(DocumentBuilder.FIELD_NAME_IDENTIFIER)
							.stringValue());
			Toast.makeText(this,hits.score(i)
					+ ": "
					+ hits.doc(i)
							.getField(DocumentBuilder.FIELD_NAME_IDENTIFIER)
							.stringValue(), Toast.LENGTH_LONG).show();

		}
    }
}